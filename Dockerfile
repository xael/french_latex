
FROM xael/latex:latest


LABEL maintainer "Alexandre NORMAN <norman@xael.org>"
LABEL description "French LaTeX compiler based on headgeekette/latex:latest"
LABEL "org.xael.vendor"="xael.org"
LABEL version="1.0"

# Packages from http://distrib-coffee.ipsl.jussieu.fr/pub/mirrors/ctan/systems/texlive/tlnet/archive/
# RUN update-tlmgr-latest.sh --update
RUN tlmgr install moreverb multirow was hyphenat aeguill framed lastpage babel-french

WORKDIR /source

ENTRYPOINT ["texliveonfly"]

