#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Invoke script for building / using french_latex container

Author: Alexandre Norman <norman () xael.org>
"""

import os
from invoke import task


@task()
def build(inv):
    """
    Build xael/french_latex:latest
    """
    inv.run(
        'cd latex_docker; make all'
    )
    inv.run(
        'docker build -t xael/french_latex:latest .'
    )


@task()
def push(inv):
    """
    Push xael/french_latex:latest
    """
    inv.run(
        'docker push xael/french_latex:latest'
    )

    
@task(help={
    'file': 'file name with or without path',
})
def latex(inv, file):
    """
    Compile latex file [file] to pdf
    """

    path, filename = os.path.split(file)
    if path in ['', '.']:
        path = './'

    path = os.path.realpath(path)

    inv.run(
        'docker run --rm -v "{}":/source latex "{}"'.format(
            path,
            filename
        )
    )
    
