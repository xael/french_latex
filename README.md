# French LaTeX docker container

Based on headgeekette/latex docker container.

This docker was created for the sole purpose of generating PDF files through texliveonfly.

The docker contains the plain TeX environment as well as the base LaTeX packages, recommended fonts, and recommended latex packages.
texliveonfly will take care of downloading any other packages needed to compile the .tex file and generate the .pdf.



## Requirements

```
pip install -r requirements
```

## Build docker image

```
inv build
inv push
```

## Usage

```
inv latex [filename]
```
or by using docker image

```
docker run --rm -v `pwd`:/source french_latex  [file.tex]
```
